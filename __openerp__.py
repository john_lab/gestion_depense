# -*- coding: utf-8 -*-
{
    'name': "Gestion_depense",

    'summary': """
        
        """,

    'description': """
       Gestion dépense mensuel
    """,

    'author': "john corp",
    'website': "http://www.john.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Béta',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}